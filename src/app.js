
import express from 'express'
import config from './config.js'
import  formularioRoute from './routes/formulario.routes.js';

const app = express()

// settings
app.set('port', config.port)

// middlewares
app.use(express.json())
app.use(express.urlencoded({ extended:false }))

app.use(formularioRoute)

export default app