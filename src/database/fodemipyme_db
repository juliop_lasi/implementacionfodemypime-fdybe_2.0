

CREATE DATABASE [Fodemipyme]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Fodemipyme', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Fodemipyme.mdf', 
	SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Fodemipyme_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Fodemipyme_log.ldf', 
	SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO

Use Fodemipyme 
go

Create table Empresa (
	idEmpresa int primary key identity (1,1) NOT NULL,
	NombreEmpresa varchar (80) NOT NULL,
	Contacto varchar (50) NOT NULL,
	Representante_legal varchar(90),
	Direccion varchar(90),
	Departamento varchar(80) NOT NULL,
	Municipio varchar(80) NOT NULL, 
	Aldea_Caserio varchar(80) NOT NULL, 
	Telefono varchar(10) NOT NULL,
	Correo varchar(80), -- UK
	Nit varchar(40),   -- UK
	fecha_inicioOperaciones date, --- Primeras inserciones de prueba
	colaboradores varchar(80),
	colaboradoras varchar(80),
	numColaboradores int,
	numColaboradoresTemp int,
	Productos_servicios varchar(200),
	-- Llaves foráneas
	id_sector int,
	id_Pueblo int,
	id_comunidad int,
	id_tipoEmpresa int,
	id_clasificacion int,
	id_edad int,
	id_Usuario int,
);
go

Alter table Empresa
add constraint fk_sector foreign key (id_sector) references Sector(id_sector)
		on update cascade
go

Alter table Empresa
add constraint fk_pueblo foreign key (id_Pueblo) references Pueblo(id_Pueblo)
		on update cascade
go

Alter table Empresa
add constraint fk_comunidad foreign key (id_comunidad) references ComunidadLinguistica(id_comunidad)
		on update cascade
go

Alter table Empresa
add constraint fk_tipoEmpresa foreign key (id_tipoEmpresa) references TipoEmpresa(id_tipoEmpresa)
		on update cascade
go

Alter table Empresa
add constraint fk_clasificacion foreign key (id_clasificacion) references Clasificacion(id_clasificacion)
		on update cascade
go

Alter table Empresa
add constraint fk_edad foreign key (id_edad) references Edad(id_edad)
		on update cascade
go

-- despues de crear tabla usuario
Alter table Empresa
add constraint fk_usuario foreign key (id_Usuario) references Usuario(id_Usuario)
		on update cascade
go

Alter table Empresa
add constraint U_correo UNIQUE (Correo)
GO

Alter table Empresa
add constraint U_nit UNIQUE (Nit)
GO

Create table Edad (
	id_edad int primary key identity(1, 1) NOT NULL,
	rango varchar(50),
);

Create table Sector (
	id_sector Int primary key identity (1, 1) NOT NULL,
	nombreSector varchar(100),
);

Create table Pueblo (
	id_Pueblo int primary key identity(1, 1) NOT NULL,
	nombrePueblo varchar(100),
);

Create table ComunidadLinguistica (
	id_comunidad int primary key identity(1, 1) NOT NULL,
	nombreComunidad varchar(100),
);

Create table TipoEmpresa (
	id_tipoEmpresa int primary key identity (1,1) NOT NULL,
	Tipo_empresa varchar(100),
	fecha date,
);

Create table Clasificacion (
	id_clasificacion int primary key identity(1, 1) NOT NULL,
	nombreClasificacion varchar(100),
);


Create table Diagnostico_Empresarial (
	id_diagnostico int primary key identity (1,1) NOT NULL,
	promedio_ventas money,
	margenGanancia int, 
	capacidad_produccion varchar(30),
	costos_produccion varchar(30),
	registros_contables varchar(50),
	Internet varchar(30),
	manejo_programas varchar(30), 
	plan_negocios varchar(20),
	necesidades_cltes varchar(20),
	local_comercial varchar(20),
	capacitaciones_prev varchar(20),
	programas_apoyo varchar(20),
	programas_ME varchar(80),
	apoyo_cooperacionInternacional varchar(20),
	cooperacion_recibida varchar(120),
	id_tipoApoyo varchar(50) NOT NULL,
	id_usuario varchar(50) NOT NULL,
	id_metodoPrecio varchar(50) NOT NULL,
	id_materialGrafico varchar(50) NOT NULL,
	id_estrategiaPromocion varchar(50) NOT NULL,
	id_medio varchar(50) NOT NULL,
	id_mercado varchar(50) NOT NULL,
	id_capacitacion int NULL,
	idEmpresa int NULL,
);

Alter table Diagnostico_Empresarial
add constraint fk_tipoApoyo foreign key (id_tipoApoyo) references TipoApoyo(id_tipoApoyo)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_usuario foreign key (id_usuario) references Usuario(id_usuario)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_metodoPrecio foreign key (id_metodoPrecio) references MetodoPrecio(id_metodoPrecio)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_materialGr foreign key (id_materialGrafico) references MaterialGrafico(id_materialGrafico)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_estrategiaP foreign key (id_estrategiaPromocion) references EstrategiaPromocion(id_estrategiaPromocion)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_medio foreign key (id_medio) references Medio(id_medio)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_mercado foreign key (id_mercado) references Mercado(id_mercado)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_capacitacion foreign key (id_capacitacion) references Capacitacion(id_capacitacion)
		on update cascade
go

Alter table Diagnostico_Empresarial
add constraint fk_empresa foreign key (idEmpresa) references Empresa(idEmpresa)
		on update cascade
		on delete no action
go

Create table MetodoPrecio (
	id_metodoPrecio int primary key identity(1, 1) NOT NULL, 
	nombreMetodo varchar(100),
);

Alter table MetodoPrecio
add constraint U_metodo UNIQUE (nombreMetodo)
GO

Create table TipoApoyo (
	id_tipoApoyo int primary key identity(1, 1) NOT NULL, 
	nombreTipo varchar(100),
);

Create table MaterialGrafico (
	id_materialGrafico int primary key identity(1, 1) NOT NULL, 
	nombreMaterial varchar(100),
);

Alter table MaterialGrafico
add constraint U_metodo UNIQUE (nombreMaterial)
GO

Create table EstrategiaPromocion (
	id_estrategiaPromocion int primary key identity(1, 1) NOT NULL, 
	nombreEstrategia varchar(100),
);

Alter table EstrategiaPromocion
add constraint U_estrategia UNIQUE (nombreEstrategia)
GO

Create table Mercado (
	id_mercado int primary key identity(1, 1) NOT NULL, 
	nombreMercado varchar(100),
);

Alter table Mercado
add constraint U_mercado UNIQUE (nombreMercado)
GO

Create table Medio (
	id_medio int primary key identity(1, 1) NOT NULL, 
	nombreMedio varchar(100),
);

Alter table Medio
add constraint U_medio UNIQUE (nombreMedio)
GO

Create table Capacitacion (
	id_capacitacion int primary key identity (1,1) NOT NULL,
	TipoCapacitacion varchar(100),
);

Create table Usuario (
	id_Usuario int primary key identity(1, 1) NOT NULL, 
	nombreUsuario varchar(100) NOT NULL,
	contra varchar(100) NOT NULL,
	id_centroAtencion varchar(120) NOT NULL,
);

Alter table Usuario
add constraint U_usuario UNIQUE (nombreUsuario)
GO

Create table CentroAtencion (
	id_centroAtencion int primary key identity(1, 1) NOT NULL, 
	nombreCentro varchar(100) NOT NULL,
	direccion varchar(100) NOT NULL,
);

Create table Empleados (
	id_Empleados int primary key identity(1, 1) NOT NULL, 
	nombreEmpleado varchar(100) NOT NULL,
	puesto varchar(100) NOT NULL,
	id_centroAtencion int,
);

Alter table Empleados
add constraint fk_centro foreign key (id_centroAtencion) references CentroAtencion(id_centroAtencion)
		on update cascade
go

Create table InicioSesion (
	id_inicioSesion int primary key identity (1,1) NOT NULL,
	fecha date NOT NULL,
	id_Usuario int,
);

Alter table InicioSesion
add constraint fk_inicio foreign key (id_Usuario) references Usuario(id_Usuario)
		on update cascade
go


Create table PlanTrabajo (
	id_plan int primary key identity(1, 1) NOT NULL, 
	id_matriz int NOT NULL,
	id_necesidades int NOT NULL,
	id_PlanComer int NOT NULL,
	idEmpresa int ,
);

Alter table PlanTrabajo
add constraint fk_matriz foreign key (id_matriz) references MatrizPlan(id_matriz)
		on update cascade
go

Alter table PlanTrabajo
add constraint fk_necesidades foreign key (id_necesidades) references NecesidadesDetectadas(id_necesidades)
		on update cascade
go

Alter table PlanTrabajo
add constraint fk_plan foreign key (id_PlanComer) references PlanComercio(id_PlanComercio)
		on update cascade
go

Alter table PlanTrabajo
add constraint fk_empresaa foreign key (idEmpresa) references Empresa(idEmpresa)
		on update cascade
		on delete no action
go

Create table NecesidadesDetectadas (
	id_necesidades int primary key identity(1, 1) NOT NULL,
	nombre varchar(100),
);

Alter table NecesidadesDetectadas
add constraint U_necesidades UNIQUE (nombre)
GO

Create table MatrizPlan (
	id_matriz int primary key identity(1, 1) NOT NULL,
	concepto varchar(120),
	tipo varchar(100),
	cant_horas int,
	fecha_inicio date,
	fecha_final date,
	costo money,
	proveedor varchar(70),
);

Create table PlanComercio (
	id_PlanComercio int primary key identity(1, 1) NOT NULL,
	nombre_de_evento varchar(120),
	lugar varchar(100),
	fecha_inicio date,
	fecha_final date,
	tipoApoyo varchar(70),
	costo money,
);