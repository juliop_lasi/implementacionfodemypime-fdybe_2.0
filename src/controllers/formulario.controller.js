import { getConnection, sql } from '../database/connection.js';
import  queries  from '../database/queries.js';

// función getOpSector es una prueba nada más, las opciones vienen desde el front end
export const getOpEmpresa = async (req, res) => {

    try {
        // Opciones de selección para el campo Sector
    const poolEmpresa = await getConnection(); //poolSector = cliente que hace consulta a la tabla Sector
    const empresa = await poolEmpresa.request().query(queries.getAllEmpresas); // luego hace el query
    console.log(empresa);

    res.send(empresa.recordset);
    } catch (error) {
        res.status(500)
        res.send(error.message) // en caso que la consulte falle desde el servidor
    }
};

// Método para insertar uno de los campos obligatorios
export const createNewEmpresa = async (req, res) => {

    // nombreSector se llama lo que viene desde el cliente contenido en req.body
    const {nombreEmpresa, Contacto, Representante_legal, Direccion, Municipio, Departamento, 
        Aldea_Caserio, Telefono, Correo, Nit, fecha_inicioOperaciones} = req.body

    // Validación de campo obligatorio
    if (nombreEmpresa == null || Contacto == null || Direccion == null || Municipio == null ||
        Departamento == null || Aldea_Caserio == null || Telefono == null || Correo == null || 
        Nit == null || fecha_inicioOperaciones == null) {
        return res.status(400).json({msg: 'Debe llenar todos los campos obligatorios'})
    }

    // Respuesta del servidor, omitir.
    console.log(nombreEmpresa)
    /**=============================**/

    try {
        const poolEmpresa = await getConnection();

        await poolEmpresa
            .request()
            .input("nombreEmpresa", sql.VarChar, nombreEmpresa)
            .input("Contacto", sql.VarChar, Contacto)
            .input("Representante_legal", sql.VarChar, Representante_legal)
            .input("Direccion", sql.VarChar, Direccion)
            .input("Municipio", sql.VarChar, Municipio)
            .input("Departamento", sql.VarChar, Departamento)
            .input("Aldea_Caserio", sql.VarChar, Aldea_Caserio)
            .input("Telefono", sql.VarChar, Telefono)
            .input("Correo", sql.VarChar, Correo)
            .input("Nit", sql.VarChar, Nit)
            .input("fecha_inicioOperaciones", sql.Date, fecha_inicioOperaciones)
            .query(queries.createNewEmpresa);

    res.json({nombreEmpresa}) // JSON hacia el cliente de lo que se guardó en la tabla
    } catch (error) {
        res.status(500)
        res.send(error.message) // en caso que la consulte falle
    }
}
